# Aplikasi Pindah SatKer
Hanya tugas magang

## Developing
1. Create file .env from .env.examples
2. Configure database in file .env
3. Run ```composer install```
4. Run ```npm install && npm run dev```
5. Run ```php artisan key:generate```
6. Run ```php artisan migrate```
7. Launch a webtest with command ```php artisan serve```